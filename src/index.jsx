import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";
import * as serviceWorker from "./serviceWorker";

ReactDOM.render(<App />, document.querySelector("#root"));

serviceWorker.register({
  onSuccess: () => {
    var elem = document.getElementById("serviceSuccess");
    elem.click();
  },
  onUpdate: reg => {
    window.registration = reg;
    var elem = document.getElementById("serviceUpdate");
    elem.click();
  }
});
