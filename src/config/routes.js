import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

export const routes = [
  {
    path: "/",
    component: Dashboard
  },
  {
    path: "/tacos",
    component: Tacos,
    routes: [
      {
        path: "/tacos/bus",
        component: Bus
      },
      {
        path: "/tacos/cart",
        component: Cart
      }
    ]
  }
];
