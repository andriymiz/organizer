import i18n from "i18next";
import LanguageDetector from "i18next-browser-languagedetector";

import langEn from "../lang/en.json";
import langUk from "../lang/uk.json";
import langRu from "../lang/ru.json";

i18n.use(LanguageDetector).init({
  detection: {
    order: ["localStorage", "navigator", "htmlTag"],
    lookupLocalStorage: "lng"
  },
  debug: false,
  fallbackLng: "en",
  keySeparator: false,
  interpolation: {
    escapeValue: false
  },
  caches: ["localStorage", "htmlTag"],
  resources: {
    en: { translation: langEn },
    uk: { translation: langUk },
    ru: { translation: langRu }
  }
});

export default i18n;
