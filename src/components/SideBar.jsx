import React, { useContext, useState } from "react";
import PropTypes from "prop-types";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import Brightness2Icon from "@material-ui/icons/Brightness2";
import Brightness7Icon from "@material-ui/icons/Brightness7";
import { Link as RouterLink } from "react-router-dom";
import { List, Collapse, useMediaQuery, Box, Avatar } from "@material-ui/core";
import { ThemeContext } from "./ThemeContextManagement";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import { makeStyles } from "@material-ui/styles";
import LanguageIcon from "@material-ui/icons/Language";
import { useTranslation } from "react-i18next";
import GetAppIcon from "@material-ui/icons/GetApp";
import LocalMoviesIcon from "@material-ui/icons/LocalMovies";
import LibraryBooksIcon from "@material-ui/icons/LibraryBooks";
import DashboardIcon from "@material-ui/icons/Dashboard";
import LockIcon from "@material-ui/icons/Lock";
import AccountBoxIcon from "@material-ui/icons/AccountBox";
import { FirebaseAuthConsumer } from "@react-firebase/auth";
import firebase from "firebase/app";
import { UIContext } from "./Pager";
import FeaturedPlayListIcon from "@material-ui/icons/FeaturedPlayList";
import AssessmentIcon from "@material-ui/icons/Assessment";
import ListAltIcon from "@material-ui/icons/ListAlt";

function ListItemLink(props) {
  const { icon, primary, to, callback } = props;

  const renderLink = React.useMemo(
    () =>
      React.forwardRef((itemProps, ref) => (
        <RouterLink to={to} ref={ref} {...itemProps} onClick={callback} />
      )),
    [to, callback]
  );

  return (
    <li>
      <ListItem button component={renderLink}>
        {icon ? <ListItemIcon>{icon}</ListItemIcon> : null}
        <ListItemText primary={primary} />
      </ListItem>
    </li>
  );
}

ListItemLink.propTypes = {
  icon: PropTypes.element,
  primary: PropTypes.string.isRequired,
  to: PropTypes.string.isRequired,
  callback: PropTypes.func
};

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    maxWidth: 380,
    backgroundColor: theme.palette.background.paper
  },
  nested: {
    paddingLeft: theme.spacing(4)
  }
}));

let deferredPrompt;

window.addEventListener("beforeinstallprompt", e => {
  deferredPrompt = e;
});

export default function SideBar() {
  const classes = useStyles();
  const theme = useContext(ThemeContext);
  const ui = useContext(UIContext);
  const [open, setOpen] = useState(false);
  const [profileMore, setProfileMore] = useState(false);
  const [mediaMore, setMediaMore] = useState(false);
  const { t, i18n } = useTranslation();
  const appIsInstalled = useMediaQuery("(display-mode: standalone)");

  return (
    <Box css={{ height: "100%" }} flexWrap="wrap">
      <Box color="text.primary" bgcolor="primary.main">
        <FirebaseAuthConsumer>
          {({ user }) => {
            return (
              <List aria-label="main">
                <ListItem>
                  <Avatar src={user.photoURL} />
                </ListItem>

                <ListItem button onClick={() => setProfileMore(!profileMore)}>
                  {user.isAnonymous ? (
                    <ListItemText
                      primary={t("Anonymous")}
                      secondary="anonymous@mail.com"
                    />
                  ) : (
                    <ListItemText
                      primary={user.displayName}
                      secondary={user.email}
                    />
                  )}
                  {profileMore ? <ExpandLess /> : <ExpandMore />}
                </ListItem>
              </List>
            );
          }}
        </FirebaseAuthConsumer>
      </Box>

      <List component="div">
        <Collapse in={profileMore} timeout="auto" unmountOnExit>
          <ListItemLink
            to="/my-accaunt"
            primary={t("My account")}
            icon={<AccountBoxIcon />}
            callback={() => ui.setDrawerState(false)}
          />
          <ListItem
            button
            onClick={e => {
              ui.setDrawerState(false);
              firebase.auth().signOut();
            }}
          >
            <ListItemIcon>
              <LockIcon />
            </ListItemIcon>
            <ListItemText primary={t("Sign out")} />
          </ListItem>
          <Divider />
        </Collapse>
        <ListItemLink
          to="/"
          primary={t("Dashboard")}
          icon={<DashboardIcon />}
          callback={() => ui.setDrawerState(false)}
        />
        <ListItemLink
          to="/books"
          primary={t("Books")}
          icon={<LibraryBooksIcon />}
          callback={() => ui.setDrawerState(false)}
        />
        <ListItem
          button
          onClick={() => {
            setMediaMore(!mediaMore);
          }}
        >
          <ListItemIcon>
            <LocalMoviesIcon />
          </ListItemIcon>
          <ListItemText primary={t("Media")} />
          {mediaMore ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
        <Collapse in={mediaMore} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            <ListItemLink
              to="/media"
              primary={t("Trands")}
              icon={<AssessmentIcon />}
              callback={() => ui.setDrawerState(false)}
            />
          </List>
          <List component="div" disablePadding>
            <ListItemLink
              to="/media/movies"
              primary={t("Movies")}
              icon={<FeaturedPlayListIcon />}
              callback={() => ui.setDrawerState(false)}
            />
          </List>
          <List component="div" disablePadding>
            <ListItemLink
              to="/media/tv-shows"
              primary={t("TV Shows")}
              icon={<FeaturedPlayListIcon />}
              callback={() => ui.setDrawerState(false)}
            />
          </List>
          <List component="div" disablePadding>
            <ListItemLink
              to="/media/in-account"
              primary={t("In account")}
              icon={<ListAltIcon />}
              callback={() => ui.setDrawerState(false)}
            />
          </List>
          <List component="div" disablePadding>
            <ListItemLink
              to="/media/my-lists"
              primary={t("My Lists")}
              icon={<ListAltIcon />}
              callback={() => ui.setDrawerState(false)}
            />
          </List>
        </Collapse>
        <Divider />
        <ListItem
          button
          onClick={e => {
            theme.setDarkMode(!theme.darkMode);
          }}
        >
          <ListItemIcon>
            {theme.darkMode ? <Brightness7Icon /> : <Brightness2Icon />}
          </ListItemIcon>
          <ListItemText
            primary={theme.darkMode ? t("Day mode") : t("Night mode")}
          />
        </ListItem>
        <ListItem
          button
          onClick={() => {
            setOpen(!open);
          }}
        >
          <ListItemIcon>
            <LanguageIcon />
          </ListItemIcon>
          <ListItemText primary={t("Language")} />
          {open ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
        <Collapse in={open} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            <ListItem
              button
              className={classes.nested}
              onClick={() => i18n.changeLanguage("en")}
            >
              <ListItemText primary="English" />
            </ListItem>
            <ListItem
              button
              className={classes.nested}
              onClick={() => i18n.changeLanguage("uk")}
            >
              <ListItemText primary="Українська" />
            </ListItem>
          </List>
        </Collapse>
        {!appIsInstalled && (
          <ListItem
            button
            onClick={e => {
              if (deferredPrompt) {
                deferredPrompt.prompt();
              }
            }}
          >
            <ListItemIcon>
              <GetAppIcon />
            </ListItemIcon>
            <ListItemText primary={t("Install App")} />
          </ListItem>
        )}
      </List>

      <Box css={{ position: "absolute", bottom: 0 }} p={2}>
        {t("Version {{v}}", { v: process.env.REACT_APP_VERSION })}
      </Box>
    </Box>
  );
}
