import React, { lazy, Suspense, useState, createContext } from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch as RouteSwitch
} from "react-router-dom";
import { SwipeableDrawer, makeStyles, createStyles } from "@material-ui/core";
import SideBar from "./SideBar";
import { useTranslation } from "react-i18next";
import Loader from "./Loader";

const AsyncDashboard = lazy(() => import("./pages/Dashboard/index.jsx"));
const AsyncBooks = lazy(() => import("./pages/Books/index.jsx"));
const AsyncMedia = lazy(() => import("./pages/Media/index.jsx"));
const AsyncMyAccaunt = lazy(() => import("./pages/MyAccaunt/index.jsx"));

const useStyles = makeStyles(theme =>
  createStyles({
    drawer: {
      width: 250
    }
  })
);

export const UIContext = createContext({
  drawerState: false,
  setDrawerState: drawerState => {}
});

const Pager = () => {
  const classes = useStyles();
  const [drawerState, setDrawerState] = useState(false);
  const { t } = useTranslation();

  const ui = {
    drawerState: drawerState,
    setDrawerState: setDrawerState
  };
  return (
    <UIContext.Provider value={ui}>
      <Router>
        <Suspense fallback={<Loader text={t("Loading...")} />}>
          <RouteSwitch>
            <Route path="/" component={AsyncDashboard} exact />
            <Route path="/books" component={AsyncBooks} />
            <Route path="/media" component={AsyncMedia} />
            <Route path="/my-accaunt" component={AsyncMyAccaunt} />
          </RouteSwitch>
        </Suspense>
        <SwipeableDrawer
          open={drawerState}
          onClose={() => setDrawerState(false)}
          onOpen={() => setDrawerState(true)}
          className={classes.drawer}
        >
          <SideBar />
        </SwipeableDrawer>
      </Router>
    </UIContext.Provider>
  );
};

export default Pager;
