import React, { useCallback, useState, useEffect, useContext } from "react";
import { Box } from "@material-ui/core";
import { TMDBContext } from ".";
import HorizontalList from "./HorizontalList";

const MyList = ({ list }) => {
  const tmdb = useContext(TMDBContext);
  const listFallback = useCallback(
    () =>
      tmdb
        .getList(list.id)
        .then(data => Promise.resolve({ results: data.items })),
    [tmdb, list.id]
  );

  return <HorizontalList title={list.name} method={listFallback} />;
};

const ContentOfMyLists = ({ hidden }) => {
  const tmdb = useContext(TMDBContext);
  const [lists, setLists] = useState({ results: [] });

  useEffect(() => {
    tmdb.getLists().then(data => {
      if (data.results) setLists(data);
    });
  }, [tmdb]);

  return (
    <Box hidden={hidden}>
      {lists.results.length > 0 &&
        lists.results.map((list, i) => <MyList key={i} list={list} />)}
    </Box>
  );
};

export default ContentOfMyLists;
