import React, { useCallback, useContext, memo } from "react";
import { Box } from "@material-ui/core";
import { useTranslation } from "react-i18next";
import { TMDBContext } from ".";
import HorizontalList from "./HorizontalList";

const ContentOfMovies = memo(({ hidden }) => {
  const tmdb = useContext(TMDBContext);
  const { t } = useTranslation();

  const nowPlayingCallback = useCallback(() => tmdb.getNowPlayingMovies(), [
    tmdb
  ]);

  const popularCallback = useCallback(() => tmdb.getPopularMovies(), [tmdb]);

  const topRatedCallback = useCallback(() => tmdb.getTopRatedMovies(), [tmdb]);

  const upcomingCallback = useCallback(() => tmdb.getUpcomingMovies(), [tmdb]);

  return (
    <Box hidden={hidden}>
      <HorizontalList title={t("Popular movies")} method={popularCallback} />
      <HorizontalList
        title={t("Now playing movies")}
        method={nowPlayingCallback}
      />
      <HorizontalList title={t("Upcoming movies")} method={upcomingCallback} />
      <HorizontalList title={t("Top rated movies")} method={topRatedCallback} />
    </Box>
  );
});

export default ContentOfMovies;
