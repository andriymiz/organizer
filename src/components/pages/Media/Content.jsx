import React, { lazy, memo } from "react";
import { Container } from "@material-ui/core";
import { Route } from "react-router";

const AsyncContentOfTvShows = lazy(() => import("./ContentOfTvShows"));
const AsyncContentOfMyLists = lazy(() => import("./ContentOfMyLists"));
const AsyncContentOfTrends = lazy(() => import("./ContentOfTrends"));
const AsyncContentOfInAccount = lazy(() => import("./ContentOfInAccount"));
const AsyncContentOfMovies = lazy(() => import("./ContentOfMovies"));

const Content = memo(() => {
  return (
    <Container component="main" disableGutters maxWidth={false}>
      <Route path="/media" component={AsyncContentOfTrends} exact />
      <Route path="/media/movies" component={AsyncContentOfMovies} />
      <Route path="/media/tv-shows" component={AsyncContentOfTvShows} />
      <Route path="/media/in-account" component={AsyncContentOfInAccount} />
      <Route path="/media/my-lists" component={AsyncContentOfMyLists} />
    </Container>
  );
});

export default Content;
