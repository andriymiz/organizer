import React, { useState, useRef, useEffect } from "react";
import {
  useMediaQuery,
  DialogContent,
  DialogTitle,
  IconButton,
  makeStyles,
  Toolbar,
  Dialog,
  AppBar,
  Popover
} from "@material-ui/core";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import MoreIcon from "@material-ui/icons/MoreVert";
import { useTheme } from "@material-ui/styles";

const useStyles = makeStyles(theme => ({
  formMenu: {
    width: 300,
    padding: theme.spacing(2),
    "&>div": {
      marginBottom: theme.spacing(2),
      width: "100%"
    }
  },
  title: {
    padding: 0
  },
  titleBar: {
    position: "relative"
  },
  content: {
    padding: 0
  }
}));

const useModalStyles = makeStyles(() => ({
  paper: {
    height: "calc(100% - 64px)"
  }
}));

const DialogBase = ({ open, setOpen, title, menuFilter, children }) => {
  const classes = useStyles();
  const modalClasses = useModalStyles();
  const theme = useTheme();
  const [menuEl, setMenuEl] = useState(null);
  const fullScreen = useMediaQuery(theme.breakpoints.down("sm"));
  const contentEl = useRef(null);

  const closeDialogHundle = () => {
    setOpen(false);
  };

  const openMenuHundle = e => {
    setMenuEl(e.currentTarget);
  };

  const closeMenuHundle = () => {
    setMenuEl(null);
  };

  useEffect(() => {
    if (contentEl && contentEl.current) contentEl.current.scrollTop = 0;
  }, [children]);

  return (
    <Dialog
      fullScreen={fullScreen}
      fullWidth={true}
      open={!!open}
      onClose={closeDialogHundle}
      scroll="paper"
      maxWidth="md"
      classes={modalClasses}
    >
      <DialogTitle disableTypography className={classes.title}>
        <AppBar className={classes.titleBar}>
          <Toolbar>
            <IconButton edge="start" onClick={closeDialogHundle}>
              <ArrowBackIcon />
            </IconButton>
            {title}
            <IconButton onClick={openMenuHundle} edge="end">
              <MoreIcon />
            </IconButton>
          </Toolbar>
          <Popover
            anchorEl={menuEl}
            open={Boolean(menuEl)}
            onClose={closeMenuHundle}
          >
            <div className={classes.formMenu}>{menuFilter}</div>
          </Popover>
        </AppBar>
      </DialogTitle>

      <DialogContent className={classes.content} ref={contentEl}>
        {children}
      </DialogContent>
    </Dialog>
  );
};

export default DialogBase;
