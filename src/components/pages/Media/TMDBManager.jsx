import React, { useState, useContext } from "react";
import {
  Dialog,
  IconButton,
  Typography,
  useMediaQuery,
  AppBar,
  Toolbar,
  List,
  ListItem,
  ListItemText,
  ListSubheader,
  Menu,
  MenuItem
} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import { useTheme } from "@material-ui/styles";
import { useTranslation } from "react-i18next";
import { TMDBContext } from ".";

const TMDBManager = ({ tmdbManager, setTMDBManager }) => {
  const theme = useTheme();
  const tmdb = useContext(TMDBContext);
  const [languageMenu, setLanguageMenu] = useState(null);
  const [countryMenu, setCountryMenu] = useState(null);
  const fullScreen = useMediaQuery(theme.breakpoints.down("sm"));
  const { t } = useTranslation();

  return (
    <Dialog
      fullScreen={fullScreen}
      open={tmdbManager}
      onClose={() => setTMDBManager(false)}
    >
      <AppBar style={{ position: "relative" }}>
        <Toolbar>
          <IconButton edge="start" onClick={() => setTMDBManager(false)}>
            <CloseIcon />
          </IconButton>
          <Typography variant="h6">{t("TMDB Manager")}</Typography>
        </Toolbar>
      </AppBar>
      <List>
        <ListSubheader>{t("TMDB Settings")}</ListSubheader>

        {tmdb.v4_account && tmdb.access_token ? (
          <ListItem
            button
            onClick={() =>
              tmdb
                .logoutV4()
                .then(() => {
                  tmdb.setV4Account(null);
                })
                .catch(() => {
                  tmdb.setV4Account(null);
                })
            }
          >
            <ListItemText
              primary={t("TMDB Account V4")}
              secondary={t("Logout") + ` (${tmdb.v4_account.id})`}
            />
          </ListItem>
        ) : (
          <ListItem
            button
            onClick={() =>
              tmdb.oAuthV4(t("TMDB Auth")).then(data => {
                tmdb.setAccessToken(data.access_token);
                tmdb.setV4Account({ id: data.account_id });
              })
            }
          >
            <ListItemText
              primary={t("TMDB Account V4")}
              secondary={t("Login")}
            />
          </ListItem>
        )}

        {tmdb.v3_account && tmdb.session_id ? (
          <ListItem
            button
            onClick={() =>
              tmdb.logout().then(() => {
                tmdb.setV3Account(null);
              })
            }
          >
            <ListItemText
              primary={t("TMDB Account V3")}
              secondary={
                t("Logout") +
                ` (${tmdb.v3_account.username}) (${tmdb.v3_account.id})`
              }
            />
          </ListItem>
        ) : (
          <ListItem
            button
            onClick={() =>
              tmdb.oAuth(t("TMDB Auth")).then(data => {
                tmdb.setSessionId(data.session_id);
                tmdb.getAccount().then(tmdb.setV3Account);
              })
            }
          >
            <ListItemText
              primary={t("TMDB Account V3")}
              secondary={t("Login")}
            />
          </ListItem>
        )}

        <ListItem
          button
          onClick={e => {
            setLanguageMenu(e.currentTarget);
            tmdb.getLanguages().then(data => tmdb.setLanguages(data));
          }}
        >
          <ListItemText
            primary={t("Language")}
            secondary={`${tmdb.language.name || tmdb.language.english_name} (${
              tmdb.language.iso_639_1
            })`}
          />
        </ListItem>

        <ListItem
          button
          onClick={e => {
            setCountryMenu(e.currentTarget);
            tmdb.getCountries().then(data => tmdb.setCountries(data));
          }}
        >
          <ListItemText
            primary={t("Country")}
            secondary={`${tmdb.country.name || tmdb.country.english_name} (${
              tmdb.country.iso_3166_1
            })`}
          />
        </ListItem>
      </List>

      <Menu
        anchorEl={languageMenu}
        keepMounted
        open={Boolean(languageMenu)}
        onClose={() => setLanguageMenu(null)}
      >
        {Boolean(languageMenu) &&
          tmdb.languages.map((option, index) => (
            <MenuItem
              key={index}
              onClick={() => {
                tmdb.setLanguage(option);
                setLanguageMenu(null);
              }}
            >
              {option.name ? option.name : option.english_name}
            </MenuItem>
          ))}
      </Menu>

      <Menu
        anchorEl={countryMenu}
        keepMounted
        open={Boolean(countryMenu)}
        onClose={() => setCountryMenu(null)}
      >
        {Boolean(countryMenu) &&
          tmdb.countries.map((option, index) => (
            <MenuItem
              key={index}
              onClick={() => {
                tmdb.setCountry(option);
                setCountryMenu(null);
              }}
            >
              {option.name ? option.name : option.english_name}
            </MenuItem>
          ))}
      </Menu>
    </Dialog>
  );
};

export default TMDBManager;
