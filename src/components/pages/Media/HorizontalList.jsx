import React, { useState, useEffect, useRef, memo } from "react";
import {
  Typography,
  Box,
  Grid,
  IconButton,
  makeStyles,
  Avatar
} from "@material-ui/core";
import HorizontalListItem from "./HorizontalListItem";
import ArrowLeftIcon from "@material-ui/icons/ArrowLeft";
import ArrowRightIcon from "@material-ui/icons/ArrowRight";

const useStyles = makeStyles(theme => ({
  grid: {
    overflowX: "scroll",
    flexWrap: "nowrap",
    "&::-webkit-scrollbar": {
      width: 0,
      height: 0
    },
    "& > div": {
      padding: theme.spacing(1)
    }
  },
  block: {
    paddingLeft: theme.props.isMobile ? 0 : theme.spacing(2),
    paddingRight: theme.props.isMobile ? 0 : theme.spacing(2),
    position: "relative"
  },
  leftArrow: {
    display: theme.props.isMobile ? "none" : "flex",
    position: "absolute",
    top: "calc(50% - 24px)",
    left: 0
  },
  rightArrow: {
    display: theme.props.isMobile ? "none" : "flex",
    position: "absolute",
    top: "calc(50% - 24px)",
    right: 0
  },
  header: {
    margin: theme.spacing(1),
    marginBottom: 0,
    marginTop: theme.spacing(2)
  }
}));

const SCROLL_WIDTH = 170 * 3;

export const HorizontalOnlyList = memo(({ title, items, type = "movie" }) => {
  const classes = useStyles();
  const gridEl = useRef(null);

  const scrollRight = () => {
    gridEl.current.scroll({
      left: gridEl.current.scrollLeft + SCROLL_WIDTH,
      behavior: "smooth"
    });
  };

  const scrollLeft = () => {
    gridEl.current.scroll({
      left: gridEl.current.scrollLeft - SCROLL_WIDTH,
      behavior: "smooth"
    });
  };

  return (
    <Box>
      <Typography variant="h6" gutterBottom className={classes.header}>
        {title}
      </Typography>
      {Array.isArray(items) && (
        <Box className={classes.block}>
          <Grid container className={classes.grid} ref={gridEl}>
            {items.length
              ? items.map((media, i) => (
                  <Grid key={i} item>
                    <HorizontalListItem media={media} type={type} />
                  </Grid>
                ))
              : Array.from(Array(5)).map((v, i) => (
                  <Grid key={i} item>
                    <HorizontalListItem type={type} />
                  </Grid>
                ))}
          </Grid>

          <Avatar className={classes.leftArrow}>
            <IconButton color="primary" onClick={scrollLeft}>
              <ArrowLeftIcon />
            </IconButton>
          </Avatar>

          <Avatar className={classes.rightArrow}>
            <IconButton color="primary" onClick={scrollRight}>
              <ArrowRightIcon />
            </IconButton>
          </Avatar>
        </Box>
      )}
    </Box>
  );
});

const HorizontalList = ({ title, method, type = "movie" }) => {
  const [list, setList] = useState([]);
  const [moTitle, setMoTitle] = useState(title);

  useEffect(() => {
    method().then(data => {
      setList(data.results);
      if (data.total_results === 0) {
        setMoTitle(
          <>
            {title}{" "}
            <Typography color="primary" variant="caption">
              List is empty
            </Typography>
          </>
        );
      }
      if (data.success === false) {
        setMoTitle(
          <>
            {title}{" "}
            <Typography color="error" variant="caption">
              {data.status_message}
            </Typography>
          </>
        );
      }
    });
  }, [method, title]);

  return <HorizontalOnlyList title={moTitle} items={list} type={type} />;
};

export default HorizontalList;
