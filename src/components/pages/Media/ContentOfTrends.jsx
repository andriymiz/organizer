import React, { useCallback, useContext } from "react";
import { Box } from "@material-ui/core";
import { useTranslation } from "react-i18next";
import { TMDBContext } from ".";
import HorizontalList from "./HorizontalList";

const ContentOfTrends = ({ hidden }) => {
  const tmdb = useContext(TMDBContext);
  const { t } = useTranslation();

  const popularMoviesCallback = useCallback(() => tmdb.getPopularMovies(), [
    tmdb
  ]);
  const popularTvShowsCallback = useCallback(() => tmdb.getPopularTvShows(), [
    tmdb
  ]);
  const topDayCallback = useCallback(() => tmdb.getTrending("all", "day"), [
    tmdb
  ]);
  const topWeekCallback = useCallback(() => tmdb.getTrending("all", "week"), [
    tmdb
  ]);

  return (
    <Box hidden={hidden}>
      <HorizontalList
        title={t("Popular movies")}
        method={popularMoviesCallback}
      />
      <HorizontalList
        title={t("Popular Tv Shows")}
        method={popularTvShowsCallback}
      />
      <HorizontalList title={t("Top day")} method={topDayCallback} />
      <HorizontalList title={t("Top week")} method={topWeekCallback} />
    </Box>
  );
};

export default ContentOfTrends;
