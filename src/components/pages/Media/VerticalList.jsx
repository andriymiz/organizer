import React, { useState, useEffect, useCallback } from "react";
import { memo } from "react";
import { Typography, Box, Button, CircularProgress } from "@material-ui/core";
import VerticalListItem from "./VerticalListItem";

export const VerticalOnlyList = memo(
  ({ text, items = Array.from(Array(5)), type = "movie" }) => {
    return (
      <Box>
        {text && (
          <Box p={1}>
            <Typography>{text}</Typography>
          </Box>
        )}
        {items.map((media, i) => (
          <Box key={i} p={1}>
            <VerticalListItem media={media} type={type} />
          </Box>
        ))}
      </Box>
    );
  }
);

let page = 0;
let isLoading = true;

const VerticalList = ({ method, type = "movie" }) => {
  const [list, setList] = useState([]);
  const [moText, setMoText] = useState("");
  const [isLoadingMore, setIsLoadingMore] = useState(true);

  const LoadMore = useCallback(() => {
    let result = method(page + 1);
    if (result instanceof Promise) {
      result.then(data => {
        isLoading = false;
        page = data.page;
        if (data.total_results === 0) {
          setMoText("empty");
        }
        setIsLoadingMore(false);
        setList(l => (page === 1 ? data.results : [...l, ...data.results]));
      });
    }
  }, [method]);

  useEffect(() => {
    LoadMore();
    return () => (page = 0);
  }, [method, LoadMore]);

  const LoadMoreHundler = () => {
    setIsLoadingMore(true);
    LoadMore();
  };

  return isLoading || page === 0 ? (
    <VerticalOnlyList type={type} />
  ) : (
    <>
      <VerticalOnlyList type={type} items={list} text={moText} />
      <Box p={1} textAlign="center">
        <Button
          onClick={LoadMoreHundler}
          disabled={isLoadingMore}
          startIcon={isLoadingMore && <CircularProgress size={16} />}
        >
          {"Load more"}
        </Button>
      </Box>
    </>
  );
};

export default VerticalList;
