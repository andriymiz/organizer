import React, { useContext, useEffect, useState } from "react";
import {
  Typography,
  Box,
  Chip,
  IconButton,
  Popover,
  CircularProgress,
  Menu,
  MenuItem
} from "@material-ui/core";
import FavoriteBorderIcon from "@material-ui/icons/FavoriteBorder";
import BookmarkBorderIcon from "@material-ui/icons/BookmarkBorder";
import StarBorderIcon from "@material-ui/icons/StarBorder";
import FavoriteIcon from "@material-ui/icons/Favorite";
import BookmarkIcon from "@material-ui/icons/Bookmark";
import ListAltIcon from "@material-ui/icons/ListAlt";
import StarIcon from "@material-ui/icons/Star";
import { Rating } from "@material-ui/lab";
import { useTranslation } from "react-i18next";
import ExterlanLinks from "./ExterlanLinks";
import { TMDBContext } from ".";
import { HorizontalOnlyList } from "./HorizontalList";

const MovieDetailsCard = ({ media }) => {
  const tmdb = useContext(TMDBContext);
  const { t } = useTranslation();
  const [item, setItem] = useState({});
  const [rateEl, setRateEl] = useState(null);
  const [listsEl, setListsEl] = useState(null);
  const [lists, setLists] = useState(null);
  const [rateValue, setRateValue] = useState(0);
  const [favorite, setFavorite] = useState(false);
  const [watchlist, setWatchlist] = useState(false);

  useEffect(() => {
    tmdb
      .getMovie(media.id, {
        append_to_response:
          "account_states,credits,external_ids,images,keywords,release_dates,videos,recommendations,similar"
      })
      .then(data => {
        if (data) {
          setItem({
            ...data,
            external_ids: { ...data.external_ids, id: media.id }
          });
          setRateValue(data.account_states.rated.value);
          setFavorite(data.account_states.favorite);
          setWatchlist(data.account_states.watchlist);
        }
      })
      .catch(() => {
        tmdb.getMovie(media.id, { cache: "force-cache" }).then(data => {
          if (data) setItem(data);
        });
      });
  }, [tmdb, media.id]);

  const markAsFavorite = () => {
    setFavorite(!favorite);

    tmdb
      .markAsFavorite(
        tmdb.v3_account.id,
        {},
        { media_type: "movie", media_id: media.id, favorite: !favorite }
      )
      .then(data => {
        if (data.status_message) alert(data.status_message);
      });
  };

  const addToWatchlist = () => {
    setWatchlist(!watchlist);

    tmdb
      .addToWatchlist(
        tmdb.v3_account.id,
        {},
        { media_type: "movie", media_id: media.id, watchlist: !watchlist }
      )
      .then(data => {
        if (data.status_message) alert(data.status_message);
      });
  };

  const rateMovie = (e, newValue) => {
    setRateValue(newValue);
    setRateEl(null);

    if (newValue) {
      tmdb.rateMovie(media.id, {}, { value: newValue }).then(data => {
        if (data.status_message) alert(data.status_message);
      });
    } else {
      tmdb.deleteMovieRating(media.id).then(data => {
        if (data.status_message) alert(data.status_message);
      });
    }
  };

  const openLists = e => {
    setListsEl(e.currentTarget);
    if (lists === null) {
      tmdb.getLists().then(data => setLists(data.results));
    }
  };

  const addToList = e => {
    setListsEl(null);
    tmdb
      .addItemsToV4List(e.currentTarget.getAttribute("listid"), {
        items: [{ media_type: "movie", media_id: media.id }]
      })
      .then(data => {
        if (data.status_message) alert(data.status_message);
      });
  };

  const releaseDate = new Date(media.release_date);

  return (
    <Box p={2}>
      <Typography variant="h5">
        {media.title} ({releaseDate.getFullYear()})
      </Typography>
      {item.tagline && `(${item.tagline})`}
      <br />
      <Typography variant="overline">
        {media.release_date}
        &nbsp;|&nbsp;
        {media.vote_average}/10 - {media.vote_count}
        &nbsp;|&nbsp;
        {item && `${item.runtime} ${t("m.")}`}
        &nbsp;|&nbsp; ${item && item.revenue}/${item && item.budget}
        &nbsp;|&nbsp;
        {item && t(item.status)}
      </Typography>
      <br />
      <img
        style={{ width: 154, height: 231 }}
        src={`https://image.tmdb.org/t/p/w154${media.poster_path}`}
        alt={media.title}
      />
      <IconButton color="primary" onClick={openLists}>
        <ListAltIcon />
      </IconButton>
      <Menu
        anchorEl={listsEl}
        keepMounted
        open={Boolean(listsEl)}
        onClose={() => setListsEl(null)}
      >
        {lists === null ? (
          <Box p={1}>
            <CircularProgress />
          </Box>
        ) : (
          lists.map(e => (
            <MenuItem
              key={e.id}
              listid={e.id}
              onClick={addToList}
            >{`${e.name} (${e.item_count})`}</MenuItem>
          ))
        )}
      </Menu>
      <IconButton color="primary" onClick={markAsFavorite}>
        {favorite ? <FavoriteIcon /> : <FavoriteBorderIcon />}
      </IconButton>
      <IconButton color="primary" onClick={addToWatchlist}>
        {watchlist ? <BookmarkIcon /> : <BookmarkBorderIcon />}
      </IconButton>
      <IconButton color="primary" onClick={e => setRateEl(e.currentTarget)}>
        {rateValue ? <StarIcon /> : <StarBorderIcon />}
      </IconButton>
      <Popover
        open={Boolean(rateEl)}
        anchorEl={rateEl}
        onClose={() => setRateEl(null)}
        anchorOrigin={{ vertical: "center", horizontal: "center" }}
        transformOrigin={{ vertical: "center", horizontal: "center" }}
      >
        <Rating
          name="rating"
          value={rateValue}
          max={10}
          onChange={rateMovie}
          size="large"
        />
      </Popover>
      <Typography variant="overline">
        {item.production_countries &&
          item.production_countries.map((item, i) => (
            <Chip key={i} size="small" label={item.name} />
          ))}
      </Typography>
      <br />
      <Typography variant="overline">
        {item.spoken_languages &&
          item.spoken_languages.map((item, i) => (
            <Chip key={i} size="small" label={item.name} />
          ))}
      </Typography>
      <br />
      <Typography variant="overline">
        {item.genres &&
          item.genres.map(item => (
            <Chip key={item.id} size="small" label={item.name} />
          ))}
      </Typography>
      <br />
      <Typography variant="overline">{media.overview}</Typography>
      <br />
      {Boolean(item.external_ids) && (
        <ExterlanLinks items={item.external_ids} />
      )}
      <HorizontalOnlyList
        title={t("Recommendations")}
        items={item.recommendations ? item.recommendations.results : []}
      />
    </Box>
  );
};

export default MovieDetailsCard;
