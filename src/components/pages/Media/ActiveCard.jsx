import React, { useContext } from "react";
import {
  IconButton,
  Typography,
  CardMedia,
  Toolbar,
  Dialog,
  AppBar,
  Slide
} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import TvShowDetailsCard from "./TvShowDetailsCard";
import MovieDetailsCard from "./MovieDetailsCard";
import { ActiveCardContext } from ".";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const ActiveCard = () => {
  const activeCard = useContext(ActiveCardContext);
  const media = activeCard.activeCardState;
  const type = media.media_type ? media.media_type : activeCard.activeCardType;

  return (
    <Dialog
      fullScreen
      open={activeCard.activeCardState ? true : false}
      onClose={() => activeCard.setActiveCardState(null)}
      TransitionComponent={Transition}
    >
      <AppBar style={{ background: "transparent", boxShadow: "none" }}>
        <Toolbar>
          <IconButton
            edge="start"
            color="inherit"
            onClick={() => activeCard.setActiveCardState(null)}
            aria-label="close"
          >
            <CloseIcon />
          </IconButton>
          <Typography variant="h6"></Typography>
        </Toolbar>
      </AppBar>
      <CardMedia
        component="img"
        image={
          "https://image.tmdb.org/t/p/w154" +
          activeCard.activeCardState.backdrop_path
        }
        style={{ width: "100%", height: 231 }}
      />
      {type === "tv" ? (
        <TvShowDetailsCard media={media} />
      ) : (
        <MovieDetailsCard media={media} />
      )}
    </Dialog>
  );
};

export default ActiveCard;
