import React, { useCallback, useContext } from "react";
import { Box } from "@material-ui/core";
import { useTranslation } from "react-i18next";
import { TMDBContext } from ".";
import HorizontalList from "./HorizontalList";

const ContentOfMyLists = ({ hidden }) => {
  const tmdb = useContext(TMDBContext);
  const { t } = useTranslation();

  const favoriteMoviesFallback = useCallback(() => tmdb.getFavoritesMovies(), [
    tmdb
  ]);
  const favoriteTvShowsFallback = useCallback(
    () => tmdb.getFavoritesTvShows(),
    [tmdb]
  );
  const movieRecommendationsFallback = useCallback(
    () => tmdb.getV4MovieRecommendations(),
    [tmdb]
  );
  const tvShowRecommendationsFallback = useCallback(
    () => tmdb.getV4TvShowRecommendations(),
    [tmdb]
  );
  const movieWatchlistFallback = useCallback(() => tmdb.getMovieWatchlist(), [
    tmdb
  ]);
  const tvShowWatchlistFallback = useCallback(() => tmdb.getTvShowWatchlist(), [
    tmdb
  ]);

  return (
    <Box hidden={hidden}>
      <HorizontalList
        title={t("Favorite movies")}
        method={favoriteMoviesFallback}
      />
      <HorizontalList
        title={t("Favorite TV shows")}
        method={favoriteTvShowsFallback}
      />
      <HorizontalList
        title={t("Movie recommendations")}
        method={movieRecommendationsFallback}
      />
      <HorizontalList
        title={t("TV shows recommendations")}
        method={tvShowRecommendationsFallback}
      />
      <HorizontalList
        title={t("Movie watchlist")}
        method={movieWatchlistFallback}
      />
      <HorizontalList
        title={t("TV show watchlist")}
        method={tvShowWatchlistFallback}
      />
    </Box>
  );
};

export default ContentOfMyLists;
