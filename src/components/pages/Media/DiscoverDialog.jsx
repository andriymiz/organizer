import React, { useState, useContext, useCallback } from "react";
import { useTranslation } from "react-i18next";
import { useLocation } from "react-router";
import {
  FormControl,
  makeStyles,
  Typography,
  InputLabel,
  MenuItem,
  Select,
  Input,
  Chip,
  TextField
} from "@material-ui/core";
import { Autocomplete } from "@material-ui/lab";
import { TMDBContext } from ".";
import VerticalList from "./VerticalList";
import DialogBase from "./DialogBase";

const useStyles = makeStyles(theme => ({
  title: {
    flexGrow: 1
  },
  chips: {
    display: "flex",
    flexWrap: "wrap"
  },
  chip: {
    margin: 2
  }
}));

const releaseTypes = [
  "Premiere",
  "Theatrical (limited)",
  "Theatrical",
  "Digital",
  "Physical",
  "TV"
];

const sortTypes = [
  "popularity.asc",
  "popularity.desc",
  "release_date.asc",
  "release_date.desc",
  "revenue.asc",
  "revenue.desc",
  "primary_release_date.asc",
  "primary_release_date.desc",
  "original_title.asc",
  "original_title.desc",
  "vote_average.asc",
  "vote_average.desc",
  "vote_count.asc",
  "vote_count.desc"
];

var years = [];
for (let index = 2022; index > 1970; index--) {
  years.push(index);
}

const DiscoverDialog = ({ open, setOpen }) => {
  const classes = useStyles();
  const tmdb = useContext(TMDBContext);
  const [dSort, setDSort] = useState("popularity.desc");
  const [dYear, setDYear] = useState(null);
  let location = useLocation();
  const [dReleaseTypes, setDReleaseTypes] = useState([]);
  const [dGenres, setDGenres] = useState([]);
  const { t } = useTranslation();

  const dType = location.pathname === "/media/tv-shows" ? "tv" : "movie";

  const discoverCallback = useCallback(
    page => {
      switch (dType) {
        case "tv":
          return tmdb.getTvDiscover({
            include_adult: true,
            sort_by: dSort,
            page: page,
            first_air_date_year: dYear ? dYear : null,
            with_genres: dGenres.length
              ? dGenres.map(v => v.id).join("|")
              : null
          });
        case "movie":
          return tmdb.getMovieDiscover({
            include_adult: true,
            sort_by: dSort,
            page: page,
            year: dYear ? dYear : null,
            with_release_type: dReleaseTypes.length
              ? dReleaseTypes.join("|")
              : null,
            with_genres: dGenres.length
              ? dGenres.map(v => v.id).join("|")
              : null
            // with_original_language: 'uk'
          });
        default:
          return () => {};
      }
    },
    [tmdb, dType, dSort, dYear, dReleaseTypes, dGenres]
  );

  return (
    <DialogBase
      open={open}
      setOpen={setOpen}
      title={
        <Typography variant="h6" className={classes.title}>
          {t(`Discover ${dType}`)}
        </Typography>
      }
      menuFilter={
        <>
          <FormControl>
            <InputLabel>{t("Sort Results By")}</InputLabel>
            <Select value={dSort} onChange={e => setDSort(e.target.value)}>
              {sortTypes.map((label, i) => (
                <MenuItem key={i} value={label}>
                  {t(label)}
                </MenuItem>
              ))}
            </Select>
          </FormControl>

          <Autocomplete
            options={years}
            getOptionLabel={option => option.toString()}
            onChange={(e, v) => setDYear(v)}
            value={dYear}
            renderInput={params => <TextField {...params} label={t("Year")} />}
          />

          <FormControl>
            <InputLabel>{t("Release Types")}</InputLabel>
            <Select
              multiple
              value={dReleaseTypes}
              onChange={e => setDReleaseTypes(e.target.value)}
              input={<Input />}
              renderValue={selected => (
                <div className={classes.chips}>
                  {selected.map(value => (
                    <Chip
                      key={value}
                      label={t(releaseTypes[value - 1])}
                      className={classes.chip}
                    />
                  ))}
                </div>
              )}
            >
              {releaseTypes.map((label, i) => (
                <MenuItem key={i} value={i + 1}>
                  {t(label)}
                </MenuItem>
              ))}
            </Select>
          </FormControl>

          <Autocomplete
            multiple
            options={dType === "tv" ? tmdb.tv_genres : tmdb.movie_genres}
            getOptionLabel={option => option.name}
            onChange={(e, v) => setDGenres(v)}
            value={dGenres}
            renderInput={params => (
              <TextField {...params} label={t("Genres")} />
            )}
          />
        </>
      }
    >
      <VerticalList method={discoverCallback} type={dType} />
    </DialogBase>
  );
};

export default DiscoverDialog;
