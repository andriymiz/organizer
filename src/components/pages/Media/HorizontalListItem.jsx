import React, { useContext } from "react";
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import { CardActionArea, Chip, makeStyles } from "@material-ui/core";
import { Skeleton } from "@material-ui/lab";
import { ActiveCardContext } from ".";

const useStyles = makeStyles(theme => ({
  chips: {
    display: "flex",
    position: "relative",
    bottom: theme.spacing(4),
    height: 0,
    justifyContent: "space-between",
    flexWrap: "wrap",
    "& > *": {
      margin: theme.spacing(0.5),
      backgroundColor: "#fffa",
      fontWeight: "bold"
    }
  }
}));

const MediaCard = ({ media, type = "movie" }) => {
  const classes = useStyles();
  const activeCard = useContext(ActiveCardContext);

  if (!media)
    return (
      <Card>
        <CardActionArea>
          <Skeleton variant="rect" height={231} width={154} />
        </CardActionArea>
      </Card>
    );

  const date = new Date(
    media.release_date ? media.release_date : media.first_air_date
  );

  const clickHundle = () => {
    activeCard.setActiveCardState(media);
    activeCard.setActiveCardType(type);
  };

  return (
    <Card>
      <CardActionArea onClick={clickHundle}>
        <CardMedia
          alt={"sdsd"}
          component="img"
          image={"https://image.tmdb.org/t/p/w154" + media.poster_path}
          style={{ width: 154, height: 231 }}
        />
        <div className={classes.chips}>
          <Chip
            variant="outlined"
            color="primary"
            size="small"
            label={media.vote_average}
          />
          <Chip
            variant="outlined"
            color="primary"
            size="small"
            label={date.getFullYear()}
          />
        </div>
      </CardActionArea>
    </Card>
  );
};

export default MediaCard;
