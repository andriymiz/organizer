import React from "react";
import {
  Dialog,
  AppBar,
  IconButton,
  Typography,
  useMediaQuery,
  withStyles
} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import { useTheme } from "@material-ui/styles";
import { useTranslation } from "react-i18next";
import MaterialTable, { MTableToolbar } from "material-table";

const StyledMTableToolbar = withStyles({
  root: {
    paddingLeft: 0,
    paddingRight: 0
  }
})(MTableToolbar);

const ListManager = ({ listManager, setListManager }) => {
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down("sm"));
  const { t } = useTranslation();

  const [state, setState] = React.useState({
    columns: [
      {
        title: "Id",
        field: "id",
        hidden: true,
        removable: false,
        grouping: false
      },
      {
        title: "Name",
        field: "name",
        initialEditValue: "New List",
        sorting: false
      },
      {
        title: "Type",
        field: "type",
        lookup: { auto: "Auto", custom: "Custom" },
        editable: "never",
        initialEditValue: "custom",
        sorting: false
      },
      {
        title: "Priority",
        field: "priority",
        type: "numeric",
        initialEditValue: 1,
        sorting: true,
        defaultSort: "asc"
      },
      {
        title: "Show",
        field: "show",
        lookup: { true: "Yes", false: "No" },
        initialEditValue: true,
        sorting: false
      }
    ],
    data: [
      { name: "Top day", id: 1, show: true, type: "auto", priority: 1 },
      { name: "Top week", id: 2, show: true, type: "auto", priority: 2 }
    ]
  });

  return (
    <Dialog
      fullScreen={fullScreen}
      open={listManager}
      onClose={() => setListManager(false)}
    >
      <MaterialTable
        title={
          <>
            <Typography variant="h6" style={{ paddingLeft: theme.spacing(2) }}>
              <IconButton edge="start" onClick={() => setListManager(false)}>
                <CloseIcon />
              </IconButton>
              {t("List manager")}
            </Typography>
          </>
        }
        columns={state.columns}
        data={state.data}
        options={{
          search: false,
          paging: false,
          actionsColumnIndex: 5,
          addRowPosition: "first",
          toolbar: true,
          draggable: false
        }}
        components={{
          Toolbar: props => (
            <AppBar style={{ position: "relative" }}>
              <StyledMTableToolbar {...props} />
            </AppBar>
          )
        }}
        editable={{
          isDeletable: rowData => rowData.type === "custom",
          onRowAdd: newData =>
            new Promise(resolve => {
              setTimeout(() => {
                resolve();
                setState(prevState => {
                  const data = [...prevState.data];
                  data.push(newData);
                  return { ...prevState, data };
                });
              }, 600);
            }),
          onRowUpdate: (newData, oldData) =>
            new Promise(resolve => {
              setTimeout(() => {
                resolve();
                if (oldData) {
                  setState(prevState => {
                    const data = [...prevState.data];
                    data[data.indexOf(oldData)] = newData;
                    return { ...prevState, data };
                  });
                }
              }, 600);
            }),
          onRowDelete: oldData =>
            new Promise(resolve => {
              setTimeout(() => {
                resolve();
                setState(prevState => {
                  const data = [...prevState.data];
                  data.splice(data.indexOf(oldData), 1);
                  return { ...prevState, data };
                });
              }, 600);
            })
        }}
      />
    </Dialog>
  );
};

export default ListManager;
