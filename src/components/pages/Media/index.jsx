import React, { useState, useEffect, useContext, createContext } from "react";
import { Toolbar } from "@material-ui/core";
import TheMovieDb from "@andriymiz/themoviedb";
import DiscoverDialog from "./DiscoverDialog";
import SearchDialog from "./SearchDialog";
import ListManager from "./ListManager";
import TMDBManager from "./TMDBManager";
import ActiveCard from "./ActiveCard";
import Content from "./Content";
import Header from "./Header";

const tmdb = new TheMovieDb(
  process.env.REACT_APP_TMDB_API_KEY,
  process.env.REACT_APP_TMDB_API_TOKEN,
  localStorage.getItem("tmdb_session_id"),
  localStorage.getItem("tmdb_access_token"),
  JSON.parse(localStorage.getItem("tmdb_v3_account")),
  JSON.parse(localStorage.getItem("tmdb_v4_account")),
  JSON.parse(localStorage.getItem("tmdb_language")),
  JSON.parse(localStorage.getItem("tmdb_country"))
);

export const TMDBContext = createContext(tmdb);

export const ActiveCardContext = createContext({
  activeCardState: null,
  activeCardType: "",
  setActiveCardState: () => {},
  setActiveCardType: () => {}
});

const Films = () => {
  const tmdb = useContext(TMDBContext);

  const loadData = async () => {
    if (!tmdb.movie_genres)
      await tmdb
        .getMovieGenres()
        .then(data => (tmdb.movie_genres = data.genres));
    if (!tmdb.tv_genres)
      await tmdb.getTvGenres().then(data => (tmdb.tv_genres = data.genres));
  };
  loadData();

  const [listManager, setListManager] = useState(false);
  const [tmdbManager, setTMDBManager] = useState(false);
  const [searchDialog, setSearchDialog] = useState(false);
  const [discoverDialog, setDiscoverDialog] = useState(false);

  const [activeCardState, setActiveCardState] = useState(null);
  const [activeCardType, setActiveCardType] = useState("movie");
  const activeCard = {
    activeCardState: activeCardState,
    activeCardType: activeCardType,
    setActiveCardState: setActiveCardState,
    setActiveCardType: setActiveCardType
  };

  const [tmdbLanguage, setTmdbLanguage] = useState(tmdb.language);
  const [tmdbLanguages, setTmdbLanguages] = useState([]);
  tmdb.language = tmdbLanguage;
  tmdb.setLanguage = setTmdbLanguage;
  tmdb.languages = tmdbLanguages;
  tmdb.setLanguages = setTmdbLanguages;
  useEffect(() => {
    localStorage.setItem("tmdb_language", JSON.stringify(tmdbLanguage));
    localStorage.setItem("tmdb_languages", JSON.stringify(tmdbLanguages));
  }, [tmdbLanguage, tmdbLanguages]);

  const [tmdbCountry, setTmdbCountry] = useState(tmdb.country);
  const [tmdbCountries, setTmdbCountries] = useState([]);
  tmdb.country = tmdbCountry;
  tmdb.setCountry = setTmdbCountry;
  tmdb.countries = tmdbCountries;
  tmdb.setCountries = setTmdbCountries;
  useEffect(() => {
    localStorage.setItem("tmdb_country", JSON.stringify(tmdbCountry));
    localStorage.setItem("tmdb_countries", JSON.stringify(tmdbCountries));
  }, [tmdbCountry, tmdbCountries]);

  const [tmdbAccessToken, setTmdbAccessToken] = useState(tmdb.access_token);
  const [tmdbSessionId, setTmdbSessionId] = useState(tmdb.session_id);
  tmdb.access_token = tmdbAccessToken;
  tmdb.setAccessToken = setTmdbAccessToken;
  tmdb.session_id = tmdbSessionId;
  tmdb.setSessionId = setTmdbSessionId;
  useEffect(() => {
    localStorage.setItem("tmdb_access_token", tmdbAccessToken);
    localStorage.setItem("tmdb_session_id", tmdbSessionId);
  }, [tmdbAccessToken, tmdbSessionId]);

  const [tmdbV3Account, setTmdbV3Account] = useState(tmdb.v3_account);
  const [tmdbV4Account, setTmdbV4Account] = useState(tmdb.v4_account);
  tmdb.v3_account = tmdbV3Account;
  tmdb.setV3Account = setTmdbV3Account;
  tmdb.v4_account = tmdbV4Account;
  tmdb.setV4Account = setTmdbV4Account;
  useEffect(() => {
    localStorage.setItem("tmdb_v3_account", JSON.stringify(tmdbV3Account));
    localStorage.setItem("tmdb_v4_account", JSON.stringify(tmdbV4Account));
  }, [tmdbV3Account, tmdbV4Account]);

  return (
    <ActiveCardContext.Provider value={activeCard}>
      <Header
        setListManager={setListManager}
        setTMDBManager={setTMDBManager}
        setSearchDialog={setSearchDialog}
        setDiscoverDialog={setDiscoverDialog}
      />
      <Toolbar />
      <Content />
      <ListManager
        setListManager={setListManager}
        listManager={listManager}
        tmdb={tmdb}
      />
      <TMDBManager setTMDBManager={setTMDBManager} tmdbManager={tmdbManager} />
      <SearchDialog open={searchDialog} setOpen={setSearchDialog} />
      <DiscoverDialog open={discoverDialog} setOpen={setDiscoverDialog} />
      {activeCardState !== null && <ActiveCard />}
    </ActiveCardContext.Provider>
  );
};

export default Films;
