import React, { useState, useContext, useCallback, useRef } from "react";
import { useTranslation } from "react-i18next";
import {
  FormControlLabel,
  FormHelperText,
  FormControl,
  RadioGroup,
  FormLabel,
  InputBase,
  Radio
} from "@material-ui/core";
import { TMDBContext } from ".";
import VerticalList from "./VerticalList";
import TextSearch from "./TextSearch";
import DialogBase from "./DialogBase";

const searchTypes = [
  {
    key: "multi",
    title: "All"
  },
  {
    key: "movie",
    title: "Movies"
  },
  {
    key: "tv",
    title: "TV Shows"
  },
  {
    key: "person",
    title: "People"
  },
  {
    key: "collection",
    title: "Collections"
  },
  {
    key: "company",
    title: "Companies"
  },
  {
    key: "keyword",
    title: "Keywords"
  }
];

const SearchDialog = ({ open, setOpen }) => {
  const tmdb = useContext(TMDBContext);
  const [searchText, setSearchText] = useState("");
  const [searchType, setSearchType] = useState("multi");
  const searchField = useRef(null);
  const { t } = useTranslation();

  const searchCallback = useCallback(
    page => {
      if (searchText.length > 0) {
        let year = searchText.match(/y:(\d\d\d\d)/);
        year = year ? year[1] : null;
        let cText = searchText.replace(/y:(\d\d\d\d)/, "");
        switch (searchType) {
          case "movie":
            return tmdb.searchMovie({
              query: cText,
              year: year,
              include_adult: true,
              page: page
            });
          case "tv":
            return tmdb.searchTv({
              query: cText,
              first_air_date_year: year,
              include_adult: true,
              page: page
            });
          case "person":
            return tmdb.searchPerson({
              query: cText,
              include_adult: true,
              page: page
            });
          case "collection":
            return tmdb.searchCollection({
              query: cText,
              include_adult: true,
              page: page
            });
          case "company":
            return tmdb.searchCompany({ query: cText, page: page });
          case "keyword":
            return tmdb.searchKeyword({ query: cText, page: page });
          default:
            return tmdb.searchMulti({ query: cText, year: year, page: page });
        }
      }
      return () => {};
    },
    [tmdb, searchText, searchType]
  );

  return (
    <DialogBase
      open={open}
      setOpen={setOpen}
      title={
        <InputBase
          inputComponent={TextSearch}
          fullWidth={true}
          placeholder={`${t("Search")} (${searchType})`}
          autoFocus={true}
          value={searchText}
          onChange={v => setSearchText(v)}
          ref={searchField}
        />
      }
      menuFilter={
        <FormControl>
          <FormLabel component="legend">{t("Search Results")}</FormLabel>
          <RadioGroup
            name="type"
            value={searchType}
            onChange={e => setSearchType(e.target.value)}
          >
            {searchTypes.map(el => (
              <FormControlLabel
                key={el.key}
                value={el.key}
                control={<Radio />}
                label={t(el.title)}
              />
            ))}
          </RadioGroup>
          <FormHelperText>
            {t(
              "Tip: You can use the 'y:' filter to narrow your results by year. Example: 'star wars y:1977'."
            )}
          </FormHelperText>
        </FormControl>
      }
    >
      {searchType === "multi" ? (
        <VerticalList method={searchCallback} />
      ) : (
        <VerticalList method={searchCallback} type={searchType} />
      )}
    </DialogBase>
  );
};

export default SearchDialog;
