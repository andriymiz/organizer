import React, { useContext } from "react";
import { Typography, CardActionArea, Card, Chip } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { ActiveCardContext } from ".";
import Grid from "@material-ui/core/Grid";
import { Skeleton } from "@material-ui/lab";

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2),
    margin: "auto",
    maxWidth: 500
  },
  image: {
    width: 92,
    height: 138,
    backgroundColor: "#333",
    display: "flex",
    alignItems: "center"
  },
  img: {
    maxHeight: "100%",
    maxWidth: "100%"
  },
  chip: {
    margin: theme.spacing(0.5),
    backgroundColor: "#fffa",
    fontWeight: "bold"
  },
  overview: {
    display: "-webkit-box",
    "-webkit-line-clamp": 3,
    "-webkit-box-orient": "vertical",
    overflow: "hidden",
    textOverflow: "ellipsis"
  },
  cardInfo: {
    paddingLeft: theme.spacing(1)
  }
}));

const MediaDetailedCard = ({ media, type = "movie" }) => {
  const classes = useStyles();
  const activeCard = useContext(ActiveCardContext);

  if (!media) {
    return (
      <CardActionArea>
        <Grid container direction="row" wrap="nowrap">
          <Grid component={Card} item className={classes.image}>
            <Skeleton variant="rect" height={200} />
          </Grid>
          <Grid item xs className={classes.cardInfo}>
            <Skeleton height={35} />
            <Skeleton width="60%" />
            <Skeleton width="60%" />
            <Skeleton width="60%" />
          </Grid>
        </Grid>
      </CardActionArea>
    );
  }
  type = media.media_type ? media.media_type : type;
  const date = new Date(
    media.release_date ? media.release_date : media.first_air_date
  );
  media.title = media.title ? media.title : media.name;
  media.poster_path = media.poster_path
    ? media.poster_path
    : media.profile_path;
  media.poster_path = media.poster_path ? media.poster_path : media.logo_path;

  let chips = [];
  if (typeof media.vote_average !== "undefined") chips.push(media.vote_average);
  if (typeof media.known_for_department !== "undefined")
    chips.push(media.known_for_department);
  if (date.getFullYear()) chips.push(date.getFullYear());
  if (media.origin_country) chips.push(media.origin_country);
  if (media.original_language) chips.push(media.original_language);
  if (media.adult) chips.push("Porn");

  const clickHundle = () => {
    activeCard.setActiveCardState(media);
    activeCard.setActiveCardType(type);
  };

  return (
    <CardActionArea onClick={clickHundle}>
      <Grid container direction="row" wrap="nowrap">
        <Grid component={Card} item className={classes.image}>
          <img
            className={classes.img}
            src={`https://image.tmdb.org/t/p/w92${media.poster_path}`}
            alt={media.title}
          />
        </Grid>
        <Grid
          item
          xs
          container
          direction="column"
          justify="space-between"
          className={classes.cardInfo}
        >
          <Grid item>
            <Typography variant="subtitle1" noWrap>
              {media.title}
            </Typography>
            <Typography variant="caption" gutterBottom>
              {type}
            </Typography>
            <Typography
              variant="caption"
              color="textSecondary"
              className={classes.overview}
            >
              {media.overview}
            </Typography>
          </Grid>
          <Grid item container>
            {chips.map((chip, i) => (
              <Chip
                key={i}
                variant="outlined"
                color="primary"
                size="small"
                className={classes.chip}
                label={chip.toString()}
              />
            ))}
          </Grid>
        </Grid>
      </Grid>
    </CardActionArea>
  );
};

export default MediaDetailedCard;
