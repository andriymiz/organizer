import React, { useCallback, useContext } from "react";
import { Box } from "@material-ui/core";
import { useTranslation } from "react-i18next";
import { TMDBContext } from ".";
import HorizontalList from "./HorizontalList";

const ContentOfTvShows = ({ hidden }) => {
  const tmdb = useContext(TMDBContext);
  const { t } = useTranslation();

  const airingTodayCallback = useCallback(() => tmdb.getAiringTodayTvShows(), [
    tmdb
  ]);

  const onTheAirCallback = useCallback(() => tmdb.getOnTheAirTvShows(), [tmdb]);

  const popularCallback = useCallback(() => tmdb.getPopularTvShows(), [tmdb]);

  const topRatedCallback = useCallback(() => tmdb.getTopRatedTvShows(), [tmdb]);

  return (
    <Box hidden={hidden}>
      <HorizontalList
        title={t("Popular TV shows")}
        method={popularCallback}
        type="tv"
      />
      <HorizontalList
        title={t("Airing today TV shows")}
        method={airingTodayCallback}
        type="tv"
      />
      <HorizontalList
        title={t("On the air TV shows")}
        method={onTheAirCallback}
        type="tv"
      />
      <HorizontalList
        title={t("Top rated TV shows")}
        method={topRatedCallback}
        type="tv"
      />
    </Box>
  );
};

export default ContentOfTvShows;
