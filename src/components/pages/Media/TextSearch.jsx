import React, { useState, useEffect } from "react";
import { IconButton } from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";

const WAIT_INTERVAL = 700;
const ENTER_KEY = 13;
let timer = null;

const TextSearch = ({ ...rest }) => {
  const [value, setValue] = useState("");

  const handleKeyDown = e => {
    if (e.keyCode === ENTER_KEY) {
      clearTimeout(timer);
      rest.onChange(value);
    }
  };

  useEffect(() => {
    clearTimeout(timer);
    timer = setTimeout(() => rest.onChange(value), WAIT_INTERVAL);
  }, [value, rest]);

  return (
    <>
      <input
        type="search"
        className={rest.className}
        placeholder={rest.placeholder}
        autoFocus={rest.autoFocus}
        value={value}
        onChange={e => setValue(e.currentTarget.value)}
        onKeyDown={handleKeyDown}
      />
      {value !== "" && (
        <IconButton onClick={() => setValue("")}>
          <CloseIcon />
        </IconButton>
      )}
    </>
  );
};

export default TextSearch;
