import React, { useState, useContext } from "react";
import {
  IconButton,
  Typography,
  makeStyles,
  MenuItem,
  Toolbar,
  AppBar,
  Menu
} from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import MoreIcon from "@material-ui/icons/MoreVert";
import MenuIcon from "@material-ui/icons/Menu";
import TuneIcon from "@material-ui/icons/Tune";
import { useTranslation } from "react-i18next";
import { UIContext } from "../../Pager";

const useStyles = makeStyles(theme => ({
  form: {
    flexGrow: 1
  },
  title: {
    flexGrow: 1
  }
}));

const Header = ({
  setListManager,
  setTMDBManager,
  setSearchDialog,
  setDiscoverDialog
}) => {
  const [menuEl, setMenuEl] = useState(null);
  const classes = useStyles();
  const { t } = useTranslation();
  const ui = useContext(UIContext);

  return (
    <AppBar>
      <Toolbar>
        <IconButton edge="start" onClick={() => ui.setDrawerState(true)}>
          <MenuIcon />
        </IconButton>
        <Typography variant="h6" className={classes.title}>
          {t("Films")}
        </Typography>
        <IconButton aria-label="search" onClick={() => setSearchDialog(true)}>
          <SearchIcon />
        </IconButton>
        <IconButton onClick={() => setDiscoverDialog(true)}>
          <TuneIcon />
        </IconButton>
        <IconButton onClick={e => setMenuEl(e.currentTarget)} edge="end">
          <MoreIcon />
        </IconButton>
      </Toolbar>
      <div>
        <Menu
          anchorEl={menuEl}
          keepMounted
          open={Boolean(menuEl)}
          onClose={() => setMenuEl(null)}
        >
          <MenuItem
            onClick={() => {
              setMenuEl(null);
              setListManager(true);
            }}
          >
            {t("Manage list")}
          </MenuItem>
          <MenuItem
            onClick={() => {
              setMenuEl(null);
              setTMDBManager(true);
            }}
          >
            {t("TMDB Settings")}
          </MenuItem>
        </Menu>
      </div>
    </AppBar>
  );
};
export default Header;
