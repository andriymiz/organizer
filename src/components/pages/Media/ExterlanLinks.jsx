import React from "react";
import { Chip, Avatar } from "@material-ui/core";

const ExterlanLinks = ({ type = "movie", items }) => {
  return (
    <>
      <Chip
        avatar={
          <Avatar src="https://www.themoviedb.org/assets/2/v4/logos/312x276-primary-green-74212f6247252a023be0f02a5a45794925c3689117da9d20ffe47742a665c518.png" />
        }
        component="a"
        href={`https://www.themoviedb.org/${type}/${items.id}`}
        clickable={true}
        label="TMDB"
      />
      <Chip
        avatar={
          <Avatar src="https://m.media-amazon.com/images/G/01/imdbpro/help/imdb-logo._CB1572911370_.png" />
        }
        component="a"
        href={`https://www.imdb.com/title/${items.imdb_id}`}
        clickable={true}
        label="IMDb"
      />
      {Boolean(items.facebook_id) && (
        <Chip
          avatar={
            <Avatar src="https://scontent.flwo2-1.fna.fbcdn.net/v/t39.2365-6/34929128_2542370199321677_3462617962773479424_n.png?_nc_cat=1&_nc_sid=ad8a9d&_nc_ohc=Qieu-gYgmN8AX9KJ1Ij&_nc_ht=scontent.flwo2-1.fna&oh=956364a02182b9cc941cda24713dfb8d&oe=5E91E052" />
          }
          component="a"
          href={`https://www.facebook.com/${items.facebook_id}`}
          clickable={true}
          label="Facebook"
        />
      )}
      {Boolean(items.instagram_id) && (
        <Chip
          avatar={
            <Avatar src="https://scontent.flwo2-1.fna.fbcdn.net/v/t39.2365-6/34644309_198434704133910_8913002448616947712_n.png?_nc_cat=1&_nc_sid=ad8a9d&_nc_ohc=sqcgB8vAUMEAX_Sqxjv&_nc_ht=scontent.flwo2-1.fna&oh=1a849a0d96c4f1e3c48200a891321417&oe=5E90F690" />
          }
          component="a"
          href={`https://www.instagram.com/${items.instagram_id}`}
          clickable={true}
          label="Instagram"
        />
      )}
      {Boolean(items.twitter_id) && (
        <Chip
          avatar={
            <Avatar src="https://scontent.flwo2-1.fna.fbcdn.net/v/t39.2365-6/34747737_1440020432811296_5160914217057910784_n.png?_nc_cat=1&_nc_sid=ad8a9d&_nc_ohc=fBe1_M6KQQUAX_SiQOL&_nc_ht=scontent.flwo2-1.fna&oh=80f8baf8789fc2a5af00c590b61b6975&oe=5E916B32" />
          }
          component="a"
          href={`https://twitter.com/${items.twitter_id}`}
          clickable={true}
          label="Twitter"
        />
      )}
    </>
  );
};

export default ExterlanLinks;
