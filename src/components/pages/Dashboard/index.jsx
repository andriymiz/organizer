import React from "react";
import { Toolbar } from "@material-ui/core";
import Header from "./Header";
import Content from "./Content";

const Dashboard = () => {
  return (
    <>
      <Header />
      <Toolbar />
      <Content />
    </>
  );
};

export default Dashboard;
