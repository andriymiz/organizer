import React, { useState, useContext } from "react";
import {
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Menu,
  MenuItem,
  makeStyles,
  createStyles
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import SearchIcon from "@material-ui/icons/Search";
import MoreIcon from "@material-ui/icons/MoreVert";
import { useTranslation } from "react-i18next";
import { UIContext } from "../../Pager";

const useStyles = makeStyles(theme =>
  createStyles({
    title: {
      flexGrow: 1
    }
  })
);

const Header = () => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = useState(null);
  const { t } = useTranslation();
  const ui = useContext(UIContext);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <AppBar>
      <Toolbar>
        <IconButton edge="start" onClick={() => ui.setDrawerState(true)}>
          <MenuIcon />
        </IconButton>
        <Typography variant="h6" className={classes.title}>
          {t("My accaunt")}
        </Typography>
        <IconButton aria-label="search">
          <SearchIcon />
        </IconButton>
        <IconButton onClick={handleClick} edge="end">
          <MoreIcon />
        </IconButton>
      </Toolbar>
      <div>
        <Menu
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
        >
          <MenuItem onClick={handleClose}>Profile</MenuItem>
          <MenuItem onClick={handleClose}>My account</MenuItem>
          <MenuItem onClick={handleClose}>Logout</MenuItem>
        </Menu>
      </div>
    </AppBar>
  );
};
export default Header;
