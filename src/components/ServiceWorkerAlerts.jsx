import React, { useState } from "react";
import i18n from "../config/i18n";
import CloseIcon from "@material-ui/icons/Close";

import { Button } from "@material-ui/core";
import Snackbar from "@material-ui/core/Snackbar";
import IconButton from "@material-ui/core/IconButton";

const updateServiceWorker = () => {
  if ("registration" in window) {
    const registrationWaiting = window.registration.waiting;
    if (registrationWaiting) {
      registrationWaiting.postMessage({ type: "SKIP_WAITING" });
      registrationWaiting.addEventListener("statechange", e => {
        if (e.target.state === "activated") {
          window.location.reload();
        }
      });
    }
  }
};

const ServiceWorkerAlerts = () => {
  const [serviceSuccess, setServiceSuccess] = useState(false);
  const [serviceUpdate, setServiceUpdate] = useState(false);

  return (
    <>
      <input
        type="hidden"
        id="serviceSuccess"
        onClick={e => setServiceSuccess(true)}
      />
      <input
        type="hidden"
        id="serviceUpdate"
        onClick={e => setServiceUpdate(true)}
      />
      <Snackbar
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center"
        }}
        open={serviceSuccess}
        autoHideDuration={13000}
        onClose={(e, reason) => {
          if (reason === "clickaway") {
            return;
          }
          setServiceSuccess(false);
        }}
        message={i18n.t("Page has been saved for offline use.")}
        action={
          <React.Fragment>
            <IconButton
              size="small"
              aria-label="close"
              color="inherit"
              onClick={() => {
                setServiceSuccess(false);
              }}
            >
              <CloseIcon fontSize="small" />
            </IconButton>
          </React.Fragment>
        }
      />
      <Snackbar
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center"
        }}
        open={serviceUpdate}
        autoHideDuration={13000}
        onClose={(e, reason) => {
          if (reason === "clickaway") {
            return;
          }
          setServiceUpdate(false);
        }}
        message={i18n.t("There is a new version {{version}} available.", {
          // version: process.env.REACT_APP_VERSION
          version: ""
        })}
        action={
          <React.Fragment>
            <Button
              color="secondary"
              size="small"
              onClick={() => updateServiceWorker()}
            >
              {i18n.t("UPDATE")}
            </Button>
            <IconButton
              size="small"
              aria-label="close"
              color="inherit"
              onClick={() => setServiceUpdate(false)}
            >
              <CloseIcon fontSize="small" />
            </IconButton>
          </React.Fragment>
        }
      />
    </>
  );
};
export default ServiceWorkerAlerts;
