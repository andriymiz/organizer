import React, { createContext, useState } from "react";

export const UIContext = createContext();

export const UIContextProvider = props => {
  const [drawerState, setDrawerState] = useState(false);

  const ui = {
    drawerState: drawerState,
    setDrawerState: setDrawerState
  };

  return <UIContext.Provider value={ui}>{props.children}</UIContext.Provider>;
};
