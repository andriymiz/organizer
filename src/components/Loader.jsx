import React from "react";
import "./Loader.css";
import logo from "../logo.svg";

const Loader = ({ text = "" }) => {
  return (
    <div className="Loader">
      <div className="Loader-header">
        <img src={logo} className="Loader-logo" alt="logo" />
        <p>{text}</p>
      </div>
    </div>
  );
};

export default Loader;
