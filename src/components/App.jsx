import React from "react";
import { ThemeContextProvider } from "./ThemeContextManagement";
import { I18nextProvider } from "react-i18next";
import i18n from "../config/i18n";
import { CssBaseline } from "@material-ui/core";
import firebaseApp from "../config/firebase";
import {
  FirebaseAuthProvider,
  FirebaseAuthConsumer
} from "@react-firebase/auth";
import ServiceWorkerAlerts from "./ServiceWorkerAlerts";
import { isNull } from "util";
import Loader from "./Loader";
import Auth from "./Auth";
import Pager from "./Pager";
import "./App.css";

const App = () => {
  return (
    <ThemeContextProvider>
      <I18nextProvider i18n={i18n}>
        <FirebaseAuthProvider firebase={firebaseApp.firebase_}>
          <CssBaseline />
          <FirebaseAuthConsumer>
            {({ isSignedIn, providerId }) => {
              if (isNull(providerId)) {
                return <Loader text={i18n.t("Initialization...")} />;
              } else if (isSignedIn) {
                return <Pager />;
              } else {
                return <Auth />;
              }
            }}
          </FirebaseAuthConsumer>
          <ServiceWorkerAlerts />
        </FirebaseAuthProvider>
      </I18nextProvider>
    </ThemeContextProvider>
  );
};

export default App;
